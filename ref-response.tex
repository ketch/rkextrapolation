\documentclass[11pt]{article}

\usepackage{color,hyperref}

\usepackage{amsmath,amsfonts,amssymb}

\setlength{\textwidth}{6.2in}
\setlength{\oddsidemargin}{0.3in}
\setlength{\evensidemargin}{0in}
\setlength{\textheight}{8.7in}
\setlength{\voffset}{-.7in}
\setlength{\headsep}{26pt}

\newcommand{\response}[1]{{\bf \color{blue} #1}\vskip 10pt}

\newcommand{\todo}[1]{{\bf \color{red} #1}\vskip 10pt}
%\newcommand{\todo}[1]{ }  % to suppress our comments

\newcommand{\done}[1]{ }  % leave in the latex but don't print

\begin{document}
We are very grateful to the referees, especially the one who prepared the
detailed report 1C with numerous helpful suggestions.
We have replied to each of the referee's points
below.  The referee's points appear in black and our responses are in blue.

As requested by the referee who prepared report 1M, we have extended the abstract 
so that it more fully describes the contents of the paper.  

\begin{itemize}

\item In their analysis in §3.1 and the Figures 3(a) and 3(b) the authors conclude that the stability region of (S)DC methods based on the explicit Euler method with θ = 1 is not growing with increasing order. This statement and result contradicts results from other studies, e.g. from reference [10] in the manuscript (see especially Figures 5.1 – 5.4). Also, the real stability interval for order p = 8 and p = 12 appear to be much smaller than what is shown in [1] (Fig. 1) for equidistant nodes. The authors should explain and comment on these disparities!


\response{
We thank the referee for pointing out this discrepancy.  Upon investigating this,
we have discovered
a bug in our code: setting $\theta=1$ resulted instead, erroneously, in use of the value $\theta=p-1$,
where $p$ is the number of nodes.  This is because a factor of $1/(p-1)$ had
been omitted in the code.  This was an extremely subtle bug because the resulting
methods still had the expected order of accuracy (as we had confirmed both by checking
the order conditions in rational arithmetic and by conducting convergence tests in floating point).
This is because the value of $\theta$ does not affect the order of accuracy.
However, it does affect the stability and accuracy properties, and using such large
values (for large $p$) led to methods with significantly worse properties
than those in the papers referred to (where the value $\theta=1$ was always used).

Figure 3(a) has been corrected.  We note that the remaining results for DC methods
were not affected by this bug since in all of them we took $\theta=0$.

We've added an acknowledgment of this specifically at the end of the paper.
}

\item E.g. the statement in the second paragraph in the conclusion that
”deferred correction seems inferior to well-designed RK methods” is too general
in my view: First of all, the author’s analysis is restricted to problems with
real eigenvalues and using forward Euler as base method in SDC. It has been
shown e.g. in [2] or [3] that semi-implicit SDC methods can perform very well
for certain types of PDEs. The drawn conclusions should be made more precise
and state only what the results presented in the paper support.


\response{
We agree that that statement, taken out of context, could be very misleading
and is unjustified.  We call the referee's attention to the text that appeared two paragraphs
later, in which we stated some of the same caveats.  We have moved that paragraph
to the beginning of the conclusion so that readers cannot possibly miss it.
We have also added the word "explicit" again at the beginning of the conclusion,
so that readers cannot possibly interpret our results as applying to semi-implicit methods.
Finally, we've included some analysis of stability for imaginary eigenvalues in the
revised manuscript.
}

\item It is shown in [1] that using a higher order method within integral deferred correction methods can improve both the order as well as the stability. Adding a DC-Midpoint to the shown examples would provide interesting additional information about this strategy and could be a valuable addition to the paper in my opinion.

\response{
We agree that that would be interesting, and we do mention it in the conclusion.
It would also be interesting to consider other variants of extrapolation.
However, given that we're already studying about 30 different methods,
we felt that adding even more would make the range of results simply overwhelming
for a single work.
Presumably, the interested reader could draw further conclusions by comparing 
works on other variants of DC to ours.
}

\item The stability analysis presented in §3.1 is only concerned with
eigenvalues on the negative real axis and neglects complex eigenvalues. This is
a rather strong limitation and I would strongly suggest to provide a stability
analysis for fully complex eigenvalues or at least give a convincing argument
why this is not done.

\response{
We have studied the full stability regions of all the methods considered, and an 
earlier version of the manuscript included imaginary axis stability intervals as well.
We have put those intervals back (in a table), with the following warning:

    ``We caution that, for high order methods, the boundary of the stability region typically
    lies very close to the imaginary axis, so values of the amplification factor may 
    differ from unity by less than $10^{-15}$ over a reasonably large interval.
    For instance, the 10th-order extrapolation method has $I_\textup{imag}=0$,
    but the magnitude of its stability polynomial differs from unity by less than
    $1.4\times 10^{-15}$ over the interval $[-i/4,i/4]$.
    It is not clear whether precise
    measures of $I_\textup{imag}$ are relevant for such methods in practical situations.''
}


\item The chosen numerical examples appear somewhat arbitrary: Why are different test problems used for the comparison of the serial (SB1, DETEST, 1D elasticity) and the parallel (N-body) implementation? And why is there a PDE example, unrelated to the stability analysis (see item above), for the serial but none for the parallel comparison? I strongly suggest to test the serial and parallel implementation of the methods on the same examples, so one can directly compare results. Also, I suggest merging these results into one Section and to directly discuss the differences emerging between the serial and parallel versions of the methods.

\item 
In §4.2, the authors refer to a non-stiff DETEST suite and provide results without any explanation what this suite does and tests. A reference is given, but if the paper presents numerical results based on this suite, it should give a description of the solved problems as well. Also, I am not sure it makes sense to lump together different problems from the test suite and just compute a mean error over multiple problems. It would be much more convincing to use one or two well-defined test cases instead.

\response{
Although there is precedent for this approach in the literature, we agree with the referee.
We have removed the detailed DETEST results, and now include only a short remark concerning them.
We have added results of another specific test case from the suite to the paper.  Now each of the
test cases (two ODE systems and 1 PDE) are compared both in serial and in parallel.  In fact, the
parallel results for the PDE problem puts DC methods in a much better light.
For the many-body problem, both serial results and parallel results are already included in the paper.
}

\item 
Give numbers to all equations, even those not referenced directly in the manuscript. This makes it easier to refer to specific equations in the paper in a discussion.

\response{Done.}

\item 
I would suggest to merge the discussion of concurrency with the analysis of the serial versions of the method into one section, so that one can find the full presentation of each method in one place.

\response{
We compromised by keeping them as separate sections, but placing "concurrency"
immediately following the serial section.
}

\item Give each of the numerical examples in §4.2 its own headline.

\response{Done.}

\item 
Figure two is very cluttered. As far as I can see, the solution at every node depends on all solutions from the previous iteration and, unless θ = 0, on its predecessor in the current iteration. This could probably be easier explained in the text.

\response{  We added the referee's explanation to the text of the caption.}

\item
In their Ex-Midpoint algorithm 2, the midpoint method is started with an initial Euler step. Thus, the overall method is probably not second order accurate and it is not immediately clear that the order of the corresponding extrapolation method still increases by two for each extrapolation step. This should be commented on in the paper.

\response{ If this criticism were true, it would represent a very serious error indeed!
    
    The referee is mistaken; this is a common misconception.
    All implementations of midpoint extrapolation start with an Euler step
    (it's perhaps more helpful to think of them as starting with 
    a step of the midpoint RK method, which corresponds to the first two stages).
    For instance, see the comment on line 744 of Hairer's odex.f.

    We think that the detailed algorithmic descriptions in our paper are worthwhile
    since they make things like this clear (and evidently this is not clear in other sources).

    We emphasize again that all methods were verified to possess the
    design order of accuracy by checking the order conditions in rational
    arithmetic {\em and} by running numerical convergence tests.
}

\item
Because the authors investigated deferred corrections for the integral
formulation of the initial value problems using spectral nodes and make no
further references to other DC methods, I suggest to use the name introduced
together with the method by Dutt et al. (reference [10] in the manuscript) and
to refer to the method as a ”spectral deferred correction method” (SDC).

\response{ Done.  }

\item
In the first paragraph of Section 5, the authors state rather generally
”Previous works that have considered exploiting concurrency in explicit
extrapolation and deferred correction methods include..”. This comment on
previous works should, in my opinion, be made more detailed and combined with
the discussion later in 5.1. In particular, a distinction between methods that
are ”parallel across the method” and ”parallel-across the steps”, see [4],
should be made: The former are concerned with reducing the per-step cost using
parallelization (as investigated in the manuscript) while the latter usually
introduce some form of iteration over multiple time-steps that can be
parallelized and thus reduce time-to-solution for the full time stepping. An
”across the
steps” parallelization of SDC (PFASST, reference [11] in the manuscript) has
been shown to be very effective in in large-scale parallel computation [5].
RIDC (ref. [5] in the manuscript), is a mix between both categories. So the
result that the pure straightforward ”across the method” parallelization of SDC
(actually, discrete Picard iteration) the author’s use in their paper is not
competitive should be distinguished from the ”across the steps” approaches. It
might be useful to put this discussion in the introduction, as it is part of
describing the context of the paper.

\response{
We have added the following text to the introduction:

We only consider parallelism ``across the method''.
Other approaches to parallelism in time often use parallelism ``across the steps'';
for instance, the parareal algorithm.  Some hybrid approaches include
PFASST [28, 11] and RIDC [5]; see also [14].
Our results should not be used to infer anything about those methods,
since we focus on a simpler approach that does not involve parallelism across multiple steps.
}

\item
In Figure 1(a), the DC-Euler method is shown for three values of $\theta$, but
in 1(b) and 1(c) only one plot for DC-Euler is shown. I suggest to either show
all three plots or at least to keep the legend consistent, so that the reader
knows which plot in 1(a) corresponds to the ones shown in (b) and (c).

\response{We now show all 3 values of $\theta$ in both plots, as well as in
            the accuracy efficiency plots.}

\item
End of section 2.2: Mention that for $\theta = 0$ the DC method is basically
just a discrete Picard iteration (isn't it?).

\response{We have added that statement that in Section 2.2.}

\item
In Section 3.1, last paragraph: I don't understand the sentence about the imaginary axis interval.

\response{We removed that sentence and wrote one that we hope is clearer, which we quoted above in
          reply to the referee's 4th comment.}

\item
State in more detail how the stability intervals are computed. I guess by solving the 1D test
equation and not by analytically computing stability functions?

\response{The stability analysis was done by computing stability functions and solving for the intersection of the stability region with the real and imaginary axes in exact algebra, with no floating point approximations.  This is now stated in the paper.}


\item
Eq. (4), p. 7 and Figure 4: How are the $C_{p+1}$ computed?

\response{The formula used is given as Eq. (10).}

\item
How is the error in Figure 7(b) measured? Is there an analytical solution for the PDE available or is this a numerically computed reference?

\response{A numerically computed reference solution is used.  This is now explained in the text.}

\item
Section 4.3 is probably be better placed in in the description of the extrapolation method. Also, the meaning of the sentence ”This can be observed by inspecting the Ex-Euler....” in the last paragraph is not clear to me, please spell out what exactly is the observation that I can make here?

\response{
We have removed this entire section, replacing it with a reference to another
work where this phenomenon (internal instability) is explained in detail.
}

\item
Figure 12: The caption should state clearly that the horizontal dashed lines
are the upper bounds for achievable speedup for the different values of p.

\response{
This information now appears in both the body text and the caption.
}

\item
In Section 6, why is dynamic scheduling used for a the OpenMP loop and is there
a performance impact compared to static scheduling?  

\response{
The paragraph describing Figure 12(b) is devoted to answering just this question.
We have now clarified that Figure 12(b) is based on static scheduling.
We also added an explanation to the end of the paragraph about Figure 12(a).
}

\end{itemize}

\end{document}


