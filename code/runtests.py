import matplotlib.pyplot as plt
import numpy as np

def plot_efficiency(methods,err,work,p,parallel=False):
    styles = ['k-D','b-p','g-s','r-o']
    plt.figure()
    plt.hold(True)
    for i in range(len(methods)):
        plt.loglog(err[i],work[i],styles[i],linewidth=3,markersize=14)

    plt.grid()
    plt.xlabel(r'Error')
    plt.ylabel('Derivative evaluations')
    if parallel:
        plt.ylim(1.e2,1.e4)
    else:
        plt.ylim(1.e2,1.e5)
    plt.hold(False)
    leg=[]
    leg.append(methods[0].name)
    leg.append('Ex-Euler '+str(p)+'('+str(p-1)+')')
    leg.append('Ex-Midpoint '+str(p)+'('+str(p-2)+')')
    leg.append('DC-Euler '+str(p)+'('+str(p-1)+')')
    plt.legend(leg,loc='best',fontsize=16)

    plt.xticks(np.array([1.e-12,1.e-10,1.e-8,1.e-6,1.e-4,1.e-2,1.e0]))
    

def remove_failed_values(work,err):
    nmeth = work.shape[0]
    ntol = work.shape[1]
    for imeth in range(nmeth):
        for itol in range(1,ntol):
            if err[imeth,itol] > 10000*err[imeth,itol-1]:
                work[imeth,itol] = work[imeth,itol-1]
                err[imeth,itol] = err[imeth,itol-1]

    return work, err

def runtests(p,problem,parallel=False,tol=None):
    from nodepy import rk, ivp, conv
    from nodepy import loadmethod

    if tol is None:
        tol = [10.**(-m) for m in range(2,12)]
    print tol

    # Load methods
    print 'constructing Ex-Euler'
    ex  = rk.extrap_pair(p);
    print 'constructing Ex-midpoint'
    exm = rk.extrap_pair(p/2,'midpoint');
    print 'constructing DC-Euler'
    dc  = rk.DC_pair(p-1,grid='cheb');

    if p == 6:
        ork = rk.loadRKM('CMR6')
    elif p == 8:
        ork = rk.loadRKM('DP8')
    elif p == 10:
        ork = loadmethod.load_rkpair_from_file('rk108curtis.txt')
    elif p == 12:
        ork = loadmethod.load_rkpair_from_file('rk129hiroshi.txt')

    if problem == 'suite':
        myivp = ivp.detest_suite_minus()
    else:
        myivp = ivp.detest(problem);

    methods = [ork,ex,exm,dc]

    [work,err]   = conv.ptest(methods,myivp,tol,verbosity=1,parallel=parallel);
    print work
    print err

    [work,err] = remove_failed_values(work,err)

    plot_efficiency(methods, err, work, p, parallel=parallel)
    
    fname = 'eff_'+problem+'_'+str(p)
    if parallel:
        fname = fname + '_par'
        plt.ylabel('Sequential derivative evaluations')
    plt.savefig(fname+'.pdf')

    return work, err

if __name__ == "__main__":
    problem = 'B1'
    for p in (6,8,10,12):
        runtests(p,problem,parallel=False)
