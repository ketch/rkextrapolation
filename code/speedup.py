# Plots speedup factor for extrap(), DC() and gbsextrap().

from nodepy import runge_kutta_method as rk
import pylab as pl
import numpy as np
import math

s_start=2
s_end=15
order = np.zeros(s_end-s_start)
speedup_gbs = np.zeros(math.ceil((s_end-s_start)/float(2)))
speedup_extrap = np.zeros(s_end-s_start)
speedup_dc = np.zeros(s_end-s_start)
order2 = np.zeros(math.ceil((s_end-s_start)/float(2)))
loop=0
for s in range(s_start,s_end):

        order[s-2]=s
	
	if s%2==0:
		order2[loop]=s
		gbs = rk.extrap_gbs(s/2)
		speedup_gbs[s/2-1] = len(gbs)/float(gbs.num_seq_dep_stages())
		loop+=1
	extrap = rk.extrap(s)
        dc = rk.DC(s-1)
	speedup_extrap[s-2] = len(extrap)/float(extrap.num_seq_dep_stages())
        speedup_dc[s-2] = len(dc)/float(dc.num_seq_dep_stages())      
pl.clf()
pl.hold(True)
pl.plot(order,speedup_extrap,'k-d')
pl.plot(order2,speedup_gbs,'b-o')
pl.plot(order,speedup_dc,'r-s')
pl.legend(('FE Extrapolation','GBS Extrapolation','FEIDC, $\\theta=0$'),loc="best")
pl.plot()
pl.xlabel('Order of ODE Solver')
pl.ylabel('Speedup factor')
#pl.title('Speedup factors for extrap(), gbsextrap() and DC()')
pl.grid()
pl.savefig('speedup.pdf')
