# Plots scaled stability region (real) for extrap(), gbsextrap() and DC() methods.



from nodepy import runge_kutta_method as rk
import pylab as pl
import numpy as np
import math 

s_start=2
s_end=15
real_stabregion_gbs = np.zeros(math.ceil((s_end-s_start)/float(2)))
real_stabregion_extrap = np.zeros(s_end-s_start)
real_stabregion_dc0 = np.zeros(s_end-s_start)
real_stabregion_dc1 = np.zeros(s_end-s_start)
real_stabregion_dc2 = np.zeros(s_end-s_start)
order = np.zeros(s_end-s_start)
order2 = np.zeros(math.ceil((s_end-s_start)/float(2)))
loop=0

for s in range(s_start,s_end):
	
	order[s-2]=s
	
	if s%2==0:
		order2[loop]=s
		gbs = rk.extrap_gbs(s/2)
		real_stabregion_gbs[s/2-1] = gbs.real_stability_interval()/float(len(gbs))
		loop+=1
	extrap = rk.extrap(s)
	dc0 = rk.DC(s-1)
        dc1 = rk.DC(s-1,0.5)
        dc2 = rk.DC(s-1,1.)
	
	real_stabregion_extrap[s-2] = extrap.real_stability_interval()/float(len(extrap))
	real_stabregion_dc0[s-2] = dc0.real_stability_interval()/float(len(dc0))
	real_stabregion_dc1[s-2] = dc1.real_stability_interval()/float(len(dc1))
	real_stabregion_dc2[s-2] = dc2.real_stability_interval()/float(len(dc2))
	
pl.clf()
pl.hold(True)
pl.plot(order,real_stabregion_extrap,'g-d')
pl.plot(order2,real_stabregion_gbs,'b--o')
pl.plot(order,real_stabregion_dc0,'r-s')
pl.plot(order,real_stabregion_dc1,'k-p')
pl.plot(order,real_stabregion_dc2,'m->')
pl.xlabel('Order of ODE Solver')
pl.ylabel('Scaled Stability Region (Real)')
pl.xticks([2,4,6,8,10,12,14])
pl.legend(('FE Extrapolation','GBS Extrapolation','FEIDC, $\\theta=0$','FEIDC, $\\theta=0.5$','FEIDC, $\\theta=1$'),loc="best")
pl.grid()
pl.ylim(0,1.2)
pl.savefig('scaledstab_real.pdf')

pl.hold(False)



