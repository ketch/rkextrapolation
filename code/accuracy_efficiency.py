from nodepy import rk, loadmethod, stability_function
import matplotlib.pyplot as plt
import numpy as np

mode = 'float'

s_start=4
s_end=12
order = range(s_start,s_end+1)

def compute_accuracy_efficiency():
    eff        = np.zeros((6,len(order)))
    eff_scaled = np.zeros((6,len(order)))

    for i,p in enumerate(order):

        print 'p =',p

        if p%2==0:
            if p == 4:
                ork = rk.loadRKM('Merson43')
            if p == 6:
                ork = rk.loadRKM('CMR6')
            elif p == 8:
                ork = rk.loadRKM('DP8')
            elif p == 10:
                ork = loadmethod.load_rkpair_from_file('rk108curtis.txt')
            elif p == 12:
                ork = loadmethod.load_rkpair_from_file('rk129hiroshi.txt')
                ork.p = 12

            print ork.name
            eff[0,i] = rk.accuracy_efficiency(ork,p=p)
            eff_scaled[0,i] = eff[0,i]

        print 'EX-E'
        exe = rk.extrap(p)
        eff[1,i] = rk.accuracy_efficiency(exe,p=p)
        eff_scaled[1,i] = eff[1,i] * len(exe)/exe.num_seq_dep_stages()

        if p%2==0:
            print 'EX-M'
            exm = rk.extrap(p/2,'midpoint')
            eff[2,i] = rk.accuracy_efficiency(exm,p=p)
            eff_scaled[2,i] = eff[2,i] * len(exm)/exm.num_seq_dep_stages()

        print 'DC0'
        dc0  = rk.DC(p-1,theta=0,grid='cheb')
        eff[3,i] = rk.accuracy_efficiency(dc0,p=p)
        eff_scaled[3,i] = eff[3,i] * len(dc0)/dc0.num_seq_dep_stages()

        from sympy import Rational
        one = Rational(1)
        print 'dchalf'
        dchalf  = rk.DC(p-1,theta=one/2,grid='cheb')
        eff[4,i] = rk.accuracy_efficiency(dchalf,p=p)
        eff_scaled[4,i] = eff[4,i] * len(dchalf)/dchalf.num_seq_dep_stages()

        print 'dc1'
        dc1  = rk.DC_pair(p-1,theta=one,grid='cheb')
        eff[5,i] = rk.accuracy_efficiency(dc1,p=p)
        eff_scaled[5,i] = eff[5,i] * len(dc1)/dc1.num_seq_dep_stages()

        print eff

    np.savetxt('acceff'+str(order[0])+str(order[-1])+'.txt',eff)
    np.savetxt('acceff_scaled'+str(order[0])+str(order[-1])+'.txt',eff_scaled)

    plot_accuracy_efficiency(eff,eff_scaled,order)


def plot_accuracy_efficiency(eff,eff_scaled,order):
    styles = ['k-D','b-p','g-s','r-o','c-^','m->']

    leg=[]
    leg.append('Reference RK')
    leg.append('Ex-Euler ')
    leg.append('Ex-Midpoint ')
    leg.append('IDC-Euler, $\\theta=0$')
    leg.append('IDC-Euler, $\\theta=1/2$')
    leg.append('IDC-Euler, $\\theta=1$')

    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],eff[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,eff[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('Accuracy efficiency ($\eta$)')
    plt.legend(leg,loc='best',fontsize=16)
    plt.savefig('acceff.pdf')


    # Scaled figures
    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],eff_scaled[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,eff_scaled[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('Parallel accuracy efficiency $(\eta s / s_{seq})$')
    plt.legend(leg,loc='best',fontsize=16)
    plt.savefig('acceff_scaled.pdf')
