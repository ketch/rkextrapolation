         program SDC_RK2

        implicit complex(8) (a - h, o - z)
        real rleft
        integer i,j,np,nnp,icase

        include 'comm_SDC'

        T = 1.D0
        xleft  = -8.D0
        xright = 1.D0
        
        nm = abs(rleft)*100

         do i=1, nm
            do j=1, nm
                fmesh (i, j) = COMPLEX(rleft+i/19.D0, rleft + j/10.D0)
           enddo
         enddo


         icase = 6
         iloop = 7
         i_pre = 1
         i_cor = 1
         i_final = 0

       ipoint(0) = 1
       ipoint(1) = 3
       ipoint(2) = 4
       ipoint(3) = 5
       ipoint(4) = 6
       ipoint(5) = 7
       ipoint(6) = 8
       ipoint(7) = 12

       do i = 1, 7
         ifort(i) = 600 + ipoint(i)
         i2fort(i) = 700 + ipoint(i)
         i3fort(i) = 900 + ipoint(i)
       enddo

        np = ipoint(icase)
        nnp = np-1

        do i = 1,nnp
            read(i3fort(icase), *) (a(i, j), j=1,np)
        end do

  
         dt = T
         dtt = dt/nnp

          do i1 = 1, nm
            do j1 = 1, nm
             fmesh_temp = fmesh(i1, j1)
             x0 = 1.
!.. initial approximation

            px(1) = x0
            qx(1) = f2(px(1))

!! Euler forward
            do i=1, nnp
                px(i+1) = px(i) + dtt * qx(i)
                qx(i+1) = f2(px(i+1))
            enddo

!.. Iteration
             do k=2, iloop

               call update_inte
               delx(1) = 0.

! Euler forward
                do i=1, nnp
                 delx(i+1) = delx(i) + dtt*(qx(i)-f2(px(i)))-(px(i+1)-px(i)-qx_int(i))
                 qx(i+1) = f2(px(i+1)+delx(i+1))
                enddo

                do i=2, np
                 px(i) = px(i) + delx(i)
                 qx(i) =f2(px(i))
                enddo
            enddo
!.. end of iteration

            x0=px(np)

            ilfort = icase * 1000 + i_pre*100 + i_cor*10 + iloop
            write(ilfort, *) REALPART(fmesh_temp), IMAGPART(fmesh_temp), zabs(x0)
! rescaled stability region			
            write(ilfort + 10000, *) REAL(fmesh_temp)/nnp, AIMAG(fmesh_temp)/nnp, abs(x0)

            enddo
          enddo

         end

        function f2(x)
          implicit complex(8) (a - h, o - z)
          include 'comm_SDC'
          f2 = x * fmesh_temp
        end
  
         subroutine update_inte
          implicit complex(8) (a - h, o - z)
         include 'comm_SDC'
             do i=1, np-1
              qx_int(i) = 0.
                do j=1, np
                 qx_int(i) = qx_int(i)+a(i, j)*qx(j)
                enddo
              qx_int(i) = qx_int(i) * dt / 2.
             enddo
          end
