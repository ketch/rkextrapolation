#This code shows crazy behavior of DC

from nodepy import runge_kutta_method as rk
from nodepy import ivp
from nodepy import conv
import pylab as pl

dc1 = rk.DC_pair(5);
dc2 = rk.DC_pair(7);
dc3 = rk.DC_pair(9);
dc4 = rk.DC_pair(11);

myivp = ivp.detest('SB1');
[work_dc1,DC1]=conv.ptest(dc1,myivp,[1.e-2,1.e-3,1.e-4,1.e-5,1.e-6,1.e-7,1.e-8,1.e-9,1.e-10,1.e-11,1e-12],verbosity=1);
[work_dc2,DC2]=conv.ptest(dc2,myivp,[1.e-2,1.e-3,1.e-4,1.e-5,1.e-6,1.e-7,1.e-8,1.e-9,1.e-10,1.e-11,1e-12],verbosity=1);
[work_dc3,DC3]=conv.ptest(dc3,myivp,[1.e-2,1.e-3,1.e-4,1.e-5,1.e-6,1.e-7,1.e-8,1.e-9,1.e-10,1.e-11,1e-12],verbosity=1);
[work_dc4,DC4]=conv.ptest(dc4,myivp,[1.e-2,1.e-3,1.e-4,1.e-5,1.e-6,1.e-7,1.e-8,1.e-9,1.e-10,1.e-11,1e-12],verbosity=1);

pl.clf()
pl.hold(True)
pl.loglog(DC1[0],work_dc1[0],'r-o',linewidth=2.5,markersize=10)
pl.loglog(DC2[0],work_dc2[0],'y-d',linewidth=2.5,markersize=10)
pl.loglog(DC3[0],work_dc3[0],'c-p',linewidth=2.5,markersize=10)
pl.loglog(DC4[0],work_dc4[0],'m-s',linewidth=2.5,markersize=10)
pl.xticks([1.e0,1.e-2,1.e-4,1.e-6,1.e-8,1.e-10,1.e-12], fontsize=15)
pl.yticks(fontsize=15)
pl.grid()
pl.xlabel('Error at $\mathbf{t}_\mathrm{\mathbf{final}}$', fontsize=15,fontweight='bold')
pl.ylabel('Derivative evaluations', fontsize=18,fontweight='bold')
pl.ylim([1.e2,1.e5])
pl.hold(False)
pl.legend(('IDC-Euler 6(5)','IDC-Euler 8(7)','IDC-Euler 10(9)','IDC-Euler 12(11)'),loc="best",prop={'size':15,'weight':'bold'})
#pl.title('ptest() on Orbit Equations SB1')
pl.savefig('DC_defectiveness.pdf')





