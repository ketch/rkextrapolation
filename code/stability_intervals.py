from nodepy import rk, loadmethod, stability_function
import matplotlib.pyplot as plt
import numpy as np

mode = 'float'

s_start=4
s_end=12
order = range(s_start,s_end+1)

def compute_stability_interval_values(do_imag=False):
    I_R = np.zeros((6,len(order)))
    I_I = np.zeros((6,len(order)))
    I_R_scaled = np.zeros((6,len(order)))
    I_I_scaled = np.zeros((6,len(order)))

    for i,p in enumerate(order):

        print 'p =',p

        if p%2==0:
            if p == 4:
                ork = rk.loadRKM('Merson43')
            if p == 6:
                ork = rk.loadRKM('CMR6')
            elif p == 8:
                ork = rk.loadRKM('DP8')
            elif p == 10:
                ork = loadmethod.load_rkpair_from_file('rk108curtis.txt')
            elif p == 12:
                ork = loadmethod.load_rkpair_from_file('rk129hiroshi.txt')

            print ork.name
            pp,q = ork.stability_function(mode=mode,formula='pow')
            I_R[0,i] = stability_function.real_stability_interval(pp,q)
            I_R_scaled[0,i] = I_R[0,i]/len(ork)
            if do_imag:
                I_I[0,i] = stability_function.imaginary_stability_interval(pp,q)
                I_I_scaled[0,i] = I_I[0,i]/len(ork)

        print 'EX-E'
        exe = rk.extrap(p)
        pp,q = exe.stability_function(mode=mode)
        I_R[1,i] = stability_function.real_stability_interval(pp,q)
        I_R_scaled[1,i] = I_R[1,i]/len(exe)
        if do_imag:
            I_I[1,i] = stability_function.imaginary_stability_interval(pp,q)
            I_I_scaled[1,i] = I_I[1,i]/len(exe)

        if p%2==0:
            print 'EX-M'
            exm = rk.extrap(p/2,'midpoint')
            pp,q = exm.stability_function(mode=mode)
            I_R[2,i] = stability_function.real_stability_interval(pp,q)
            I_R_scaled[2,i] = I_R[2,i]/len(exm)
            if do_imag:
                I_I[2,i] = stability_function.imaginary_stability_interval(pp,q)
                I_I_scaled[2,i] = I_I[2,i]/len(exm)

        print 'DC0'
        dc0  = rk.DC(p-1,theta=0,grid='cheb')
        pp,q = dc0.stability_function(mode=mode,formula='pow',use_butcher=True)
        try:
            I_R[3,i] = stability_function.real_stability_interval(pp,q)
            if do_imag:
                I_I[3,i] = stability_function.imaginary_stability_interval(pp,q)
        except:
            pass

        I_R_scaled[3,i] = I_R[3,i]/len(dc0)
        I_I_scaled[3,i] = I_I[3,i]/len(dc0)

        from sympy import Rational
        one = Rational(1)
        print 'dchalf'
        dchalf  = rk.DC(p-1,theta=one/2,grid='cheb')
        pp,q = dchalf.stability_function(mode='float',formula='pow',use_butcher=True)
        try:
            I_R[4,i] = stability_function.real_stability_interval(pp,q)
            if do_imag:
                I_I[4,i] = stability_function.imaginary_stability_interval(pp,q)
        except:
            pass
        I_R_scaled[4,i] = I_R[4,i]/len(dchalf)
        I_I_scaled[4,i] = I_I[4,i]/len(dchalf)

        print 'dc1'
        dc1  = rk.DC_pair(p-1,theta=one,grid='cheb')
        pp,q = dc1.stability_function(mode='float',formula='pow',use_butcher=True)
        try:
            I_R[5,i] = stability_function.real_stability_interval(pp,q)
            if do_imag:
                I_I[5,i] = stability_function.imaginary_stability_interval(pp,q)
        except:
            pass
        I_R_scaled[5,i] = I_R[5,i]/len(dc1)
        I_I_scaled[5,i] = I_I[5,i]/len(dc1)

        print I_I

    np.savetxt('real'+str(order[0])+str(order[-1])+'.txt',I_R)
    np.savetxt('imag'+str(order[0])+str(order[-1])+'.txt',I_I)
    np.savetxt('reals'+str(order[0])+str(order[-1])+'.txt',I_R_scaled)
    np.savetxt('imags'+str(order[0])+str(order[-1])+'.txt',I_I_scaled)
    plot_stability_interval_values(I_R,I_I,I_R_scaled,I_I_scaled,order)


def plot_stability_interval_values(I_R,I_I,I_R_scaled,I_I_scaled,order):
    styles = ['k-D','b-p','g-s','r-o','c-^','m->']


    leg=[]
    leg.append('Reference RK')
    leg.append('Ex-Euler ')
    leg.append('Ex-Midpoint ')
    leg.append('IDC-Euler, $\\theta=0$')
    leg.append('IDC-Euler, $\\theta=1/2$')
    leg.append('IDC-Euler, $\\theta=1$')

    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],I_R[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,I_R[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('Real stability interval')
    plt.legend(leg,loc='best')
    plt.ylim((0.0,20))
    plt.savefig('real_stability_intervals.pdf')


    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],I_I[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,I_I[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('Imaginary stability interval')
    plt.legend(leg,loc='best')
    plt.savefig('imaginary_stability_intervals.pdf')

    # Scaled figures
    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],I_R_scaled[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,I_R_scaled[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('$I_R/s$')
    plt.legend(leg,loc='best')
    plt.ylim((0.0,1.0))
    plt.savefig('real_stability_intervals_scaled.pdf')

    plt.figure()
    plt.hold(True)
    for i in range(6):
        if (i ==0) or (i == 2):
            plt.plot(order[::2],I_I_scaled[i,::2],styles[i],linewidth=3,markersize=14)
        else:
            plt.plot(order,I_I_scaled[i,:],styles[i],linewidth=3,markersize=14)
    plt.hold(False)
    plt.grid()
    plt.xlabel('Order (p)')
    plt.xticks([4,6,8,10,12])
    plt.ylabel('$I_I/s$')
    plt.legend(leg,loc='best')
    plt.savefig('imaginary_stability_intervals_scaled.pdf')
